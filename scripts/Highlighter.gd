extends Node2D

func _unhandled_input(event : InputEvent):
	if not event is InputEventMouse:
		return
		
	global_position = HexTileMap.axial_to_global(HexTileMap.global_to_axial(get_global_mouse_position()))
