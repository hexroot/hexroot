class_name Const

enum HexDirection  {
	UP         = 0b000001,
	UP_RIGHT   = 0b000010,
	DOWN_RIGHT = 0b000100,
	DOWN       = 0b001000,
	DOWN_LEFT  = 0b010000,
	UP_LEFT    = 0b100000,
}

const HexDirections : = PoolIntArray([
	HexDirection.UP,
	HexDirection.UP_RIGHT,
	HexDirection.DOWN_RIGHT,
	HexDirection.DOWN,
	HexDirection.DOWN_LEFT,
	HexDirection.UP_LEFT,
])

const BASE_ANGLE : float = PI/3
const EPSILON : float = 0.1

static func angle_from_direction(direction: int) -> float:
	var mod : int = 0
	if (direction == HexDirection.UP):
		mod = 0
	elif (direction == HexDirection.UP_RIGHT):
		mod = 1
	elif (direction == HexDirection.DOWN_RIGHT):
		mod = 2
	elif (direction == HexDirection.DOWN):
		mod = 3
	elif (direction == HexDirection.DOWN_LEFT):
		mod = 4
	elif (direction == HexDirection.UP_LEFT):
		mod = 5
	return mod * BASE_ANGLE

static func opposite(direction: int) -> int:
	match direction:
		HexDirection.UP:
			return HexDirection.DOWN
		HexDirection.UP_RIGHT:
			return HexDirection.DOWN_LEFT
		HexDirection.DOWN_RIGHT:
			return HexDirection.UP_LEFT
		HexDirection.DOWN:
			return HexDirection.UP
		HexDirection.DOWN_LEFT:
			return HexDirection.UP_RIGHT
		HexDirection.UP_LEFT:
			return HexDirection.DOWN_RIGHT
	return 0
