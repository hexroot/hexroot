extends Node

class_name EnemyManager

signal enemy_turn_ended
signal enemy_spawned

export var spawn_chance : float = 0.2

onready var enemy_prefab = get_child(0)
onready var core : = get_tree().root.find_node("Player", true, false)
onready var spawn_tiles : = ring_from(core.axial_pos, 6)

var is_enemy_turn : bool

var enemy_list : Array
var cur_enemy_id : int
var is_waiting_for_enemy : bool

func _ready():
	enemy_list = []
	is_enemy_turn = false

static func ring_from(axial: Vector2, radius: int) -> PoolVector2Array:
	var ret : = PoolVector2Array()
	var hex : = axial + Vector2(-1, 1) * radius
	for dir in Const.HexDirections:
		for step in range(radius):
			ret.append(hex)
			hex = HexTileMap.neighbour(hex, dir)
	return ret

func begin_turn():
	is_enemy_turn = true
	try_generate_enemy()
	cur_enemy_id = 0
	is_waiting_for_enemy = false
	set_enemies_direction()

func do_turn() -> bool:
	if (cur_enemy_id >= enemy_list.size()):
		return false
	
	if (is_waiting_for_enemy):
		return true
	
	is_waiting_for_enemy = enemy_list[cur_enemy_id].do_turn()
	
	if (not is_waiting_for_enemy):
		cur_enemy_id += 1
	
	return true

func set_enemies_direction():
	for enemy in enemy_list:
		var direction = get_direction_to_core(enemy)
		enemy.move_toward_direction(direction)

func get_direction_to_core(enemy) -> int:
	var dir : Vector2 = core.axial_pos - HexTileMap.global_to_axial(enemy.global_position)
	if dir.x < dir.y:
		dir = Vector2(clamp(dir.x, -1, 0), clamp(dir.y, 0, 1))
	else:
		dir = Vector2(clamp(dir.x, 0, 1), clamp(dir.y, -1, 0))
	return HexTileMap.direction_from_axial(dir)

func try_generate_enemy():
	if (randf() > spawn_chance):
		return
	
	var new_enemy = enemy_prefab.duplicate()
	new_enemy.visible = true
	var position = get_random_position()
	new_enemy.current_position = position
	new_enemy.position = HexTileMap.axial_to_global(position)
	new_enemy.connect("move_ended", self, "_on_single_enemy_move_ended")
	new_enemy.connect("killed", self, "_on_single_enemy_killed")
	add_child(new_enemy)
	enemy_list.append(new_enemy)
	emit_signal("enemy_spawned", new_enemy)

func _on_single_enemy_move_ended():
	is_waiting_for_enemy = false

func _on_single_enemy_killed(enemy: Node2D):
	enemy_list.remove(enemy_list.find(enemy))
	enemy.queue_free()
	
func get_random_position() -> Vector2:
	return spawn_tiles[randi() % spawn_tiles.size()]

func _physics_process(_delta):
	if (not is_enemy_turn):
		return
	
	var new_state : = do_turn()
	if (new_state != is_enemy_turn && new_state == false):
		emit_signal("enemy_turn_ended", enemy_list)
	is_enemy_turn = new_state

func _on_player_turn_ended():
	begin_turn()
