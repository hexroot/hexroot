extends Node2D

class_name Enemy

export var facing = Const.HexDirection.UP
export var speed : float

export var target_position : Vector2
var current_position : Vector2
export var moving : bool

export var max_life : int = 6
onready var LifeGauge : TextureProgress = $LifeGauge

signal move_to
signal move_ended
signal killed

func _ready():
	current_position = HexTileMap.global_to_axial(global_position)
	target_position = current_position
	moving = false
	LifeGauge.max_value = max_life
	LifeGauge.value = max_life

func take_damage(damage_value: int):
	LifeGauge.value -= damage_value
	if (LifeGauge.value == 0):
		die()

func die():
	emit_signal("killed", self)

func move_toward_direction(direction : int):
	facing = direction
	target_position = HexTileMap.neighbour(current_position, direction)

func move_to_target():
	var movement : Vector2 = (HexTileMap.axial_to_global(target_position) - position)
	if (movement.length() < speed-Const.EPSILON):
		position = HexTileMap.axial_to_global(target_position)
		current_position = target_position
		return
	translate(speed*movement.normalized())

func process_move():
	if (not moving):
		return
	
	if (target_position != current_position):
		move_to_target()
	else:
		moving = false
		emit_signal("move_ended")

func do_turn():
	if (not moving && target_position != current_position):
		moving = true
		emit_signal("move_to", target_position, self)
	return moving

func _physics_process(_delta):
	process_move()
	get_node("Sprite").rotation = Const.angle_from_direction(facing)

