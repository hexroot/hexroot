extends TileMap

class_name HexTileMap

const SQRT_3 : float = sqrt(3)
const HEX_SIZE : int = 64
const HEX_WIDTH : int= 2*HEX_SIZE
const HEX_HEIGHT : float = SQRT_3*HEX_SIZE
const TILE_WIDTH : float = 97.0
const TILE_HEIGHT : float = 110.0

static func cube_to_axial(cube_coord : Vector3) -> Vector2:
	return Vector2(cube_coord.x, cube_coord.y)

static func axial_to_cube(axial_coord : Vector2) -> Vector3:
	return Vector3(axial_coord.x, axial_coord.y, -axial_coord.x-axial_coord.y)

static func cube_round(floating_coord : Vector3) -> Vector3:
	var rounded_flat : Vector3 = floating_coord.round();
	var diff : Vector3 = (rounded_flat - floating_coord).abs();
	
	if (diff.x > diff.y && diff.x > diff.z):
		rounded_flat.x = -rounded_flat.y - rounded_flat.z
	elif (diff.y > diff.z):
		rounded_flat.y = -rounded_flat.x - rounded_flat.z
	else:
		rounded_flat.z = -rounded_flat.x - rounded_flat.y
	
	return rounded_flat

static func axial_round(floating_coord : Vector2) -> Vector2:
	return cube_to_axial(cube_round(axial_to_cube(floating_coord)))

static func global_to_cube(global_pos : Vector2) -> Vector3:
	var local_pos : Vector2 = global_pos - Vector2(TILE_WIDTH/2, TILE_HEIGHT/2)
	var q : float = local_pos.x / TILE_WIDTH
	var r : float = -local_pos.x / (2*TILE_WIDTH) + local_pos.y / TILE_HEIGHT
	return cube_round(axial_to_cube(Vector2(q,r)))

static func global_to_axial(global_pos : Vector2) -> Vector2:
	return cube_to_axial(global_to_cube(global_pos))

static func axial_to_global(axial_pos : Vector2) -> Vector2:
	var x : float = TILE_WIDTH * axial_pos.x
	var y : float = TILE_HEIGHT * axial_pos.x / 2 + TILE_HEIGHT * axial_pos.y
	return Vector2(x,y) + Vector2(TILE_WIDTH/2, TILE_HEIGHT/2)

static func cube_length(cube_difference: Vector3) -> float:
	var absolute : = cube_difference.abs()
	return absolute[absolute.max_axis()]

static func direction_from_cube(cube_vec: Vector3) -> int:
	return direction_from_axial(cube_to_axial(cube_vec))

static func direction_from_axial(axial: Vector2) -> int:
	if axial == Vector2(0, 1):
		return Const.HexDirection.DOWN
	if axial == Vector2(-1, 1):
		return Const.HexDirection.DOWN_LEFT
	if axial == Vector2(-1, 0):
		return Const.HexDirection.UP_LEFT
	if axial == Vector2(0, -1):
		return Const.HexDirection.UP
	if axial == Vector2(1, -1):
		return Const.HexDirection.UP_RIGHT
	if axial == Vector2(1, 0):
		return Const.HexDirection.DOWN_RIGHT
	return 0

func cube_to_grid(cube_pos: Vector3) -> Vector2:
	return axial_to_grid(cube_to_axial(cube_pos))

func axial_to_grid(axial_pos : Vector2) -> Vector2:
	return Vector2(axial_pos.x, axial_pos.y + (axial_pos.x - (int(axial_pos.x)&1)) / 2)

static func axial_distance(a: Vector2, b: Vector2):
	var vec : = a - b
	return (abs(vec.x)
		  + abs(vec.x + vec.y)
		  + abs(vec.y)) / 2

static func neighbour(axial_position : Vector2, direction : int) -> Vector2:
	var neighbour_position = axial_position
	if (direction == Const.HexDirection.UP):
		neighbour_position.y -= 1
	elif (direction == Const.HexDirection.UP_RIGHT):
		neighbour_position.x += 1
		neighbour_position.y -= 1
	elif (direction == Const.HexDirection.DOWN_RIGHT):
		neighbour_position.x += 1
	elif (direction == Const.HexDirection.DOWN):
		neighbour_position.y += 1
	elif (direction == Const.HexDirection.DOWN_LEFT):
		neighbour_position.x -= 1
		neighbour_position.y += 1
	elif (direction == Const.HexDirection.UP_LEFT):
		neighbour_position.x -= 1
	return neighbour_position

func global_to_grid(global_pos : Vector2) -> Vector2:
	return axial_to_grid(global_to_axial(global_pos))
