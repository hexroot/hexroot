extends Node2D

class_name Tower

onready var animationPlayer : AnimationPlayer = $AnimationPlayer
onready var LifeGauge : TextureProgress = $LifeGauge

func _ready():
	animationPlayer.play("Mushroom_Start",-1)

func _on_AnimationPlayer_animation_finished(anim_name):
	animationPlayer.play("mushroomIdle")

func attack():
	animationPlayer.play("mushroomAttack")

func take_damage(damage_value: int) -> float:
	LifeGauge.value -= damage_value
	return LifeGauge.value
