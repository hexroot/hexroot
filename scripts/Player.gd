extends Node2D

onready var sprite = $HeartCell
onready var cam = $Camera2D
onready var LifeGauge : TextureProgress = $LifeGauge
onready var axial_pos : = HexTileMap.global_to_axial(sprite.global_position)

func _ready():
	sprite.global_position = HexTileMap.axial_to_global(axial_pos)

func _process(_delta):
	cam.global_position = lerp(sprite.global_position, get_global_mouse_position(), 0.45)

func take_damage(damage_value: int) -> float:
	LifeGauge.value -= damage_value
	return LifeGauge.value
