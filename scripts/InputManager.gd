extends Node2D

export (PackedScene) var Tower

signal player_turn_ended

const NOT_SELECTED : = Vector2(INF, INF)
const NAV_INDEX_COORD_BITS : = 0xF # Above that and the AStar refuses the generated indices...
const TRUNCATION_MASK : = ~(-1 << NAV_INDEX_COORD_BITS)

onready var Terrain : HexTileMap = $Terrain
onready var Roots : TileMap = $Roots
onready var rootTiles : TileSet = Roots.tile_set
onready var Selector : Node2D = $Selector
onready var Player : = $Player
onready var astar_node : = AStar2D.new()

var selected : = NOT_SELECTED
var dirToIndex : = Dictionary()
var your_turn : = true
var TowerPosArr : = PoolVector2Array()
var TowerArr : = Array()

# The direction bitmask is encoded as a character in ['!'; '_']
func dir_from_index(index: int) -> int:
	return rootTiles.tile_get_name(index).ord_at(0) - 32

# Deterministicly get an index from coordinates
# We do this by packing the Vec2 into the int
static func nav_index_from_grid(pos: Vector2) -> int:
	return (
		   (int(pos.x) & TRUNCATION_MASK)
		| ((int(pos.y) & TRUNCATION_MASK) << NAV_INDEX_COORD_BITS)
	)

func _ready():
	var indices : Array = rootTiles.get_tiles_ids()
	for index in indices:
		dirToIndex[dir_from_index(index)] = index
	
	var hardcoded_starting_roots : = [Vector2(4, 2), Vector2(4, 3)]
	for p in hardcoded_starting_roots:
		astar_node.add_point(nav_index_from_grid(p), p)
	astar_node.connect_points(
		nav_index_from_grid(hardcoded_starting_roots[0]),
		nav_index_from_grid(hardcoded_starting_roots[1])
	)

func add_link(tilemap_pos: Vector2, direction: int):
	var tile_index : int = Roots.get_cellv(tilemap_pos)
	if tile_index != TileMap.INVALID_CELL:
		direction |= dir_from_index(tile_index)
	var expected : = char(32 + direction)
	Roots.set_cellv(tilemap_pos, dirToIndex[direction])

func unselect():
	selected = NOT_SELECTED
	Selector.visible = false

func end_turn():
	unselect()
	your_turn = false
	emit_signal("player_turn_ended")

func _unhandled_input(event : InputEvent):
	if (event is InputEventMouseButton):
		process_click_event(event)
		
func process_click_event(event : InputEventMouseButton):
	if not your_turn:
		return
	if (not(event.is_pressed())):
		return
	
	if event.button_index == BUTTON_RIGHT:
		unselect()
		return

	if event.button_index != BUTTON_LEFT:
		return

	var axial_pos : Vector2 = Terrain.global_to_axial(get_global_mouse_position())
	if axial_pos in TowerPosArr:
		return # Prevent linking towers
		
	var tilemap_pos : Vector2 = Terrain.axial_to_grid(axial_pos)
	var cellIndex : int = Roots.get_cellv(tilemap_pos)
	if (selected == NOT_SELECTED
	&&  cellIndex != TileMap.INVALID_CELL
	&&  !astar_node.get_id_path(
			nav_index_from_grid(tilemap_pos),
			nav_index_from_grid(Terrain.axial_to_grid(Player.axial_pos))
		).empty()
	):
		selected = axial_pos
		Selector.global_position = Terrain.axial_to_global(axial_pos)
		Selector.visible = true
		return # Parent cell selected
		
	if selected == NOT_SELECTED:
		return # No parent cell selected, cancel
	
	if axial_pos == selected:
		if selected == Player.axial_pos:
			return
		
		TowerPosArr.append(axial_pos)
		var tower : Node2D = Tower.instance()
		tower.global_position = HexTileMap.axial_to_global(axial_pos)
		add_child(tower)
		TowerArr.append(tower)
		end_turn()
		return
	
	var dir : = HexTileMap.direction_from_axial(selected - axial_pos)
	if dir == 0:
		return
	
	add_link(tilemap_pos, dir)
	var parent_tilemap_pos : Vector2 = Terrain.axial_to_grid(selected)
	add_link(parent_tilemap_pos, Const.opposite(dir))
	var new_nav_point_index : = nav_index_from_grid(tilemap_pos)
	astar_node.add_point(new_nav_point_index, tilemap_pos)
	astar_node.connect_points(new_nav_point_index, nav_index_from_grid(parent_tilemap_pos))
	end_turn()

func _on_enemy_turn_ended(enemy_list: Array):
	for enemy in enemy_list:
		var axial_pos : Vector2 = enemy.current_position
		for index in range(TowerPosArr.size()):
			if HexTileMap.axial_distance(axial_pos, TowerPosArr[index]) < 3:
				TowerArr[index].attack()
				enemy.take_damage(1)
	your_turn = true


func _on_enemy_spawned(enemy: Enemy):
	enemy.connect("move_to", self, "_on_enemy_move_to")

func _on_enemy_move_to(axial: Vector2, enemy: Enemy):
	var tilemap_pos : = Terrain.axial_to_grid(axial)
	var tile_index : int = Roots.get_cellv(tilemap_pos)
	var tower_idx : = TowerPosArr.find(axial)
	if (tile_index == TileMap.INVALID_CELL
	 && tower_idx == -1
	 && axial != Player.axial_pos):
		return # Let it move
	
	# Make it attack instead
	enemy.moving = false
	
	if axial == Player.axial_pos:
		if Player.take_damage(1) <= 0:
			get_tree().reload_current_scene() # You lose
		return
	
	# Damage tower
	if 0 <= tower_idx:
		var tower : Tower = TowerArr[tower_idx]
		if tower.take_damage(1) <= 0:
			TowerArr.remove(tower_idx)
			TowerPosArr.remove(tower_idx)
			tower.queue_free()
		return
	
	# Cut roots
	astar_node.remove_point(nav_index_from_grid(tilemap_pos))
	Roots.set_cellv(tilemap_pos, TileMap.INVALID_CELL)
	var dirs: = dir_from_index(tile_index)
	for dir in Const.HexDirections:
		if dirs & dir:
			var neighbour : = Terrain.axial_to_grid(Terrain.neighbour(axial, dir))
			var neighbour_dirs : = dir_from_index(Roots.get_cellv(neighbour)) & ~Const.opposite(dir)
			var index : = TileMap.INVALID_CELL
			if neighbour_dirs:
				index = dirToIndex[neighbour_dirs]
			Roots.set_cellv(neighbour, index)
